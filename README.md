This is a project structure for developing the Foundational Capability in the Digital 2.0 Platform for AEM based applications. This structure is also intended as a template based on the best practice set of examples and adobe recommendations. 


## Modules

This structure includes several modules which serve a very specific purpose. Every project should start off by creating all the modules and then chose to ignore or remove any project that is not needed. The modules of the structure are:

* core: this module will hold the Java bundle containing all core functionality like OSGi services, listeners or schedulers, as well as component-related Java code such as servlets or request filters.
* ui.apps: this module contains the /apps (and /etc) parts of the project. This module is responsible essentially responsible for holding the components and templates.
* ui.content: this module contains sample content using the components from the ui.apps which can be used for various forms of testing like unit, integration, functional.
* ui.config: this module contains confgigurations for the services defined in this project. The configurations re treated like content packaging but have been abstracted out so that it allows us to manage the configuration deployment by environment easily without keeping any dependencies on code and hence making our releases relatively easy.
* ui.tests: this module contains Java bundle containing JUnit tests that are executed server-side. This bundle is not to be deployed onto production. any client side "in container" Junit types of unit tests should be created in the core module.
* ui.launcher: this module contains glue code that deploys the ui.tests bundle (and dependent bundles) to the server and triggers the remote JUnit execution. 

Not all the modules are to be deployed on all environments and we shoudl look carefully when doing deployments. For example, we would never want to deploy the ui.launcher, ui.tests, ui.content on QA, Stage or Production environments.


## How to build

To build all the modules run in the project root directory the following command with Maven 3:

    mvn clean install

If you have a running AEM instance you can build and package the whole project and deploy into AEM with  

    mvn clean install -PautoInstallPackage
    
Or to deploy it to a publish instance, run

    mvn clean install -PautoInstallPackagePublish
    
Or to deploy only the bundle to the author, run

    mvn clean install -PautoInstallBundle


## Testing

There are two levels of testing contained in the project:

* unit test in core: this show-cases classic unit testing of the code contained in the bundle. To test, execute:

    mvn clean test

* server-side integration tests: this allows to run unit-like tests in the AEM-environment, ie on the AEM server. To test, execute:

    mvn clean integration-test -PintegrationTests
